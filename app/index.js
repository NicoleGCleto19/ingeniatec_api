const express = require('express');
const app = express();
const http = require('http');
const server = http.createServer(app);
const port = 3001;

// hardcoded users
const users = require('../users.json')

app.get('/', (req, res) => {
 res.status(200).send(`Alive - Ingeniatec Test`);
});

app.get('/users', function (req, res) {
    res.send(users.users)
  })

server.listen(port, () => {
 console.log(`Server running at port ${port}`);
});